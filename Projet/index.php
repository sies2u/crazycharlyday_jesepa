<?php
// Mise en place de l'autoload
require_once 'vendor/autoload.php';
use CrazyCharlyDay\Modele as m;
use CrazyCharlyDay\Controleur as c;
use CrazyCharlyDay\Vue as v;
use Illuminate\Database\Capsule\Manager as DB;
use Slim\Slim as s;

// La session commence lorsque l'utilisateur se connecte sur le site
session_start();

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

// On déclare la variable SLIM pour créer les routes
$app = new s();
$GLOBALS['app'] = $app;

// Affichage de la page d'accueil
$GLOBALS['app']->get('/', function(){
  c\ControleurAccueil::pageAccueil();

})->name("accueil");
// Affichage des items d'une catégorie
$GLOBALS['app']->get('/categorie/:id', function($id){
  c\ControleurCategorie::visualiserItems($id);
})->name("visualiser");

$GLOBALS['app']->get('/item/:id', function($id){
  c\ControleurItem::visualiserItem($id);
})->name("item");

$GLOBALS['app']->get('/Inscription' , function(){
    c\ControleurCompte::pageInscription();
})->name("inscription");

$GLOBALS['app']->post('/Inscription' , function() {
  c\ControleurCompte::testInscription();
})->name("test_inscription") ;

$GLOBALS['app']->get('/Connexion' , function(){
    c\ControleurCompte::pageConnexion();
})->name("connexion");

$GLOBALS['app']->post('/Connexion' , function() {
  c\ControleurCompte::testConnexion();
})->name("test_connexion") ;

$GLOBALS['app']->get('/Utilisateurs' , function(){
  c\ControleurCompte::visualiserUtilisateur();
})->name("utilisateurs") ;

$GLOBALS['app']->get('/Deconnexion' , function(){
    c\ControleurCompte::deconnexion();
})->name("deconnexion");

$GLOBALS['app']->get('/Reserver/:id', function(){
  c\ControleurCalendar::reserver();
})->name("reservation");

$GLOBALS['app']->get('/Compte/Modification' , function(){
    if(c\Authentification::verificationConnexion()){
      c\ControleurCompte::pageModification();
    }
    else{
      echo "vous n'êtes pas connecté" ;
    }
})->name("compte_modification");

$GLOBALS['app']->post('/Compte/Modification' , function(){
  c\ControleurCompte::testModification() ;
})->name("test_compte_modification");

$GLOBALS['app']->get('/Calendar' , function(){
    c\ControleurCalendar::pageCalendar();
})->name("calendar");

// On lance ensuite slim
$GLOBALS['app']->run();
