<?php
namespace CrazyCharlyDay\Vue;
use CrazyCharlyDay\Modele as m;
use CrazyCharlyDay\Controleur as c;
class VueItem extends Vue{

  private $item;
  private $html;
  private $content;
  private $navbar;

  public function __construct($item){
    $this->item = $item;
  }

  public function visualiserItem(){
    $item = $this->item;
    $url = $GLOBALS['app']->urlfor('accueil')."img/item/$item->id.jpg";
    $contenu = "<center><p>$item->nom $item->description</p>
    <img src=$url></br>
    <a href=''>Liste des réservations</a><br>
    <a href=''>Planning</a><br>
    <a href=''>Formulaire de réservation</a></center></br></br>";
    if(c\Authentification::verificationConnexion()){
      $lien = $this->voirCategorie($item->id_categ);
      $this->navbar = <<<END
      <li><a href=$this->url_accueil>Accueil</a></li>
      <li><a href=$lien>Catégorie</a></li>
      <li><a href=$this->url_deconnexion>Deconnexion</a></li>
END;
    }else{
      $lien = $this->voirCategorie($item->id_categ);
      $this->navbar = <<<END
      <li><a href=$this->url_accueil>Accueil</a></li>
      <li><a href=$lien>Catégorie</a></li>
      <li><a href=$this->url_connexion>Connexion</a></li>
      <li><a href=$this->url_inscription>Inscription</a></li>
END;
    }
    return $contenu;
  }

  public function render($numero){
    switch($numero){
      case 1:
      $this->content = self::visualiserItem();
      break;
    }
    $user = "";
    if(c\Authentification::verificationConnexion()){
      $user = $_SESSION['login'];
    }else{
      $user = "Garage Planning Manager";
    }

    $html = <<<END
    <!DOCTYPE HTML>
    <html>
      <head>
        <title>Garage Planning Manager</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lte IE 8]><script src="../www/assets/js/ie/html5shiv.js"></script><![endif]-->
        <link rel="stylesheet" href="../www/assets/css/main.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="../www/assets/css/ie9.css" /><![endif]-->
        <!--[if lte IE 8]><link rel="stylesheet" href="../www/assets/css/ie8.css" /><![endif]-->
      </head>
      <body>

        <!-- Page Wrapper -->
          <div id="page-wrapper">

            <!-- Header -->
              <header id="header" class="alt">
                <h1><a>$user</a></h1>
                <nav>
                  <a href="#menu">Menu</a>
                </nav>
              </header>

            <!-- Menu -->
              <nav id="menu">
                <div class="inner">
                  <h2>Menu</h2>
                  <ul class="links">
                    $this->navbar
                  </ul>
                  <a href="#" class="close">Close</a>
                </div>
              </nav>

            <!-- Banner -->
              <section id="banner">
                <div class="inner">
                  <div class="logo"><span class="icon fa-car"></span></div>
                  <h2>Garage Planning Manager</h2>
                </div>
              </section>

            <!-- Wrapper -->
              <section id="wrapper">

                $this->content

              </section>

          </div>

        <!-- Scripts -->
          <script src="../www/assets/js/skel.min.js"></script>
          <script src="../www/assets/js/jquery.min.js"></script>
          <script src="../www/assets/js/jquery.scrollex.min.js"></script>
          <script src="../www/assets/js/util.js"></script>
          <!--[if lte IE 8]><script src="../www/assets/js/ie/respond.min.js"></script><![endif]-->
          <script src="../www/assets/js/main.js"></script>

      </body>
    </html>
END;
    print $html;
  }
}
?>
