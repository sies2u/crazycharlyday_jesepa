<?php
namespace CrazyCharlyDay\Vue;
class Vue{
    protected $home,
    $url_accueil,
    $url_connexion,
    $url_inscription,
    $url_deconnexion,
    $url_compte_modification,
    $url_utilisateurs,
    $url_calendar,
    $url_reserver;
    public function __construct(){
      $this->home = $GLOBALS['app']->urlFor("accueil");
      $this->url_accueil = $GLOBALS['app']->urlFor('accueil')  ;
      $this->url_connexion = $GLOBALS['app']->urlFor('connexion')  ;
      $this->url_inscription = $GLOBALS['app']->urlFor('inscription')  ;
      $this->url_deconnexion = $GLOBALS['app']->urlFor('deconnexion')  ;
      $this->url_compte_modification = $GLOBALS['app']->urlFor('compte_modification');
      $this->url_utilisateurs = $GLOBALS['app']->urlFor('utilisateurs');
      $this->url_calendar = $GLOBALS['app']->urlFor('calendar');
      $this->url_reserver = $GLOBALS['app']->urlFor('reservation');
    }
    public function voirCategorie($id){
      return $GLOBALS['app']->urlFor("visualiser", ['id'=>$id]);
    }
    public function voirItem($id){
      return $GLOBALS['app']->urlFor("item",['id'=>$id]);
    }
}
 ?>
