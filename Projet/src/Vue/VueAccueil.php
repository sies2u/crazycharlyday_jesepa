<?php
namespace CrazyCharlyDay\Vue;

use CrazyCharlyDay\Controleur as c ;

class VueAccueil extends Vue{
  private $html;
  private $content;
  private $navbar;
  private $header;
  private $liste;
  public function __construct($liste){
    parent::__construct();
    $this->liste = $liste;
  }
  public function render(){
    $this->content = "";
    $this->nombre=$this->random(10,22);
    foreach ($this->liste as $value) {
      $nom = $value["nom"];
      $description = $value["description"];
      $id = $value["id"];
      $url = $this->voirCategorie($id);
      $this->content = $this->content.<<<END
           							<section id="one" class="wrapper spotlight style2">
          								<div class="inner">
          									<a href="#" class="image"><img src="img/item/$this->nombre.jpg" alt="" /></a>
          									<div class="content">
          										<h2 class="major">$nom</h2>
                              <p>$description</p>
          										<a href="$url" class="special">Voir plus</a>
          									</div>
          								</div>
          							</section>
END;
    $this->nombre=$this->random(1,9);
    }
    $choix = <<<END
    <li><a href=$this->url_accueil>Accueil</a></li>
    <li><a href=$this->url_utilisateurs>Utilisateurs</a></li>
    <li><a href=$this->url_calendar>Calendrier</a></li>
    <li><a href=$this->url_connexion>Connexion</a></li>
    <li><a href=$this->url_inscription>Inscription</a></li>
END;

    if(c\Authentification::verificationConnexion()){
      $choix = <<<END
      <li><a href=$this->url_accueil>Accueil</a></li>
      <li><a href=$this->url_utilisateurs>Utilisateurs</a></li>
      <li><a href=$this->url_reserver>Reserver</a></li>
      <li><a href=$this->url_compte_modification>Modification Compte</a></li>
      <li><a href=$this->url_deconnexion>Déconnexion</a></li>
END;
    }

    $user = "";
    if(c\Authentification::verificationConnexion()){
      $user = $_SESSION['login'];
    }else{
      $user = "Garage Planning Manager";
    }


    $this->html = <<<END
    <!DOCTYPE HTML>
    <html>
    	<head>
    		<title>Garage Planning Manager</title>
    		<meta charset="utf-8" />
    		<meta name="viewport" content="width=device-width, initial-scale=1" />
    		<!--[if lte IE 8]><script src="www/assets/js/ie/html5shiv.js"></script><![endif]-->
    		<link rel="stylesheet" href="www/assets/css/main.css" />
    		<!--[if lte IE 9]><link rel="stylesheet" href="www/assets/css/ie9.css" /><![endif]-->
    		<!--[if lte IE 8]><link rel="stylesheet" href="www/assets/css/ie8.css" /><![endif]-->
    	</head>
    	<body>

    		<!-- Page Wrapper -->
    			<div id="page-wrapper">

    				<!-- Header -->
    					<header id="header" class="alt">
    						<h1><a>$user</a></h1>
    						<nav>
    							<a href="#menu">Menu</a>
    						</nav>
    					</header>

    				<!-- Menu -->
    					<nav id="menu">
    						<div class="inner">
    							<h2>Menu</h2>
    							<ul class="links">
    								$choix
    							</ul>
    							<a href="#" class="close">Close</a>
    						</div>
    					</nav>

    				<!-- Banner -->
    					<section id="banner">
    						<div class="inner">
    							<div class="logo"><span class="icon fa-car"></span></div>
    							<h2>Garage Planning Manager</h2>
    							<p>Site de réservation hebdomadaire d'ateliers et de véhicules</p>
    						</div>
    					</section>

    				<!-- Wrapper -->
    					<section id="wrapper">

                $this->content

    					</section>

    			</div>

    		<!-- Scripts -->
    			<script src="www/assets/js/skel.min.js"></script>
    			<script src="www/assets/js/jquery.min.js"></script>
    			<script src="www/assets/js/jquery.scrollex.min.js"></script>
    			<script src="www/assets/js/util.js"></script>
    			<!--[if lte IE 8]><script src="www/assets/js/ie/respond.min.js"></script><![endif]-->
    			<script src="www/assets/js/main.js"></script>

    	</body>
    </html>
END;
    print $this->html;
  }

    public function random($a,$b){
    return rand($a, $b);
  }
}
 ?>
