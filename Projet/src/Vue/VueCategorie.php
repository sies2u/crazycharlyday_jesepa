<?php
namespace CrazyCharlyDay\Vue;
use CrazyCharlyDay\Modele as m;
use CrazyCharlyDay\Controleur as c;
class VueCategorie extends Vue{

  private $html;
  private $content;
  private $items;
  private $navbar;
  private $categorie;

  public function __construct($categorie,$items){
    parent::__construct();
    $this->categorie = $categorie;
    $this->items = $items;
  }

  public function visualiserItems(){
    $categorie = $this->categorie;

    $contenu = "<section style=\"width:80%;padding-left:10%;padding-right:10%\"><h2><b>Liste : $categorie->nom </br>$categorie->description</b></h2>";
    foreach($this->items as $i){
      $lien = $this->voirItem($i->id);
      $image = <<<END

        <a href="#"><img src="../img/item/$i->id.jpg" alt="" /></a>
END;
      $contenu = $contenu."<p><b>Nom : </b></br>$i->nom</p><p><br>$image</br><b>Description:</b></br>$i->description</p><p><a href=\"$lien\">Voir plus</a>";
    }
    $contenu = $contenu."</section>";
    return $contenu;
  }

  public function render($numero){
    switch($numero){
      case 1:
      $this->content = self::visualiserItems();
      break;
    }

    if(c\Authentification::verificationConnexion()){
      $this->navbar = <<<END
      <li><a href="$this->home">Accueil</a></li>
      <li><a href="$this->url_deconnexion">Deconnexion</a></li>
END;
    }else{
      $this->navbar = <<<END
      <li><a href="$this->url_accueil">Accueil</a></li>
      <li><a href="$this->url_connexion">Connexion</a></li>
      <li><a href="$this->url_inscription">Inscription</a></li>
END;
    }

    $user = "";
    if(c\Authentification::verificationConnexion()){
      $user = $_SESSION['login'];
    }else{
      $user = "Garage Planning Manager";
    }

    $html = <<<END
    <!DOCTYPE HTML>
    <html>
      <head>
        <title>Garage Planning Manager</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lte IE 8]><script src="../www/assets/js/ie/html5shiv.js"></script><![endif]-->
        <link rel="stylesheet" href="../www/assets/css/main.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="../www/assets/css/ie9.css" /><![endif]-->
        <!--[if lte IE 8]><link rel="stylesheet" href="../www/assets/css/ie8.css" /><![endif]-->
      </head>
      <body>

        <!-- Page Wrapper -->
          <div id="page-wrapper">

            <!-- Header -->
              <header id="header" class="alt">
                <h1><a>$user</a></h1>
                <nav>
                  <a href="#menu">Menu</a>
                </nav>
              </header>

            <!-- Menu -->
              <nav id="menu">
                <div class="inner">
                  <h2>Menu</h2>
                  <ul class="links">
                    $this->navbar
                  </ul>
                  <a href="#" class="close">Close</a>
                </div>
              </nav>

            <!-- Banner -->
              <section id="banner">
                <div class="inner">
                  <div class="logo"><span class="icon fa-car"></span></div>
                  <h2>Garage Planning Manager</h2>
                </div>
              </section>

            <!-- Wrapper -->
              <section id="wrapper">

                $this->content

              </section>

          </div>

        <!-- Scripts -->
          <script src="../www/assets/js/skel.min.js"></script>
          <script src="../www/assets/js/jquery.min.js"></script>
          <script src="../www/assets/js/jquery.scrollex.min.js"></script>
          <script src="../www/assets/js/util.js"></script>
          <!--[if lte IE 8]><script src="../www/assets/js/ie/respond.min.js"></script><![endif]-->
          <script src="../www/assets/js/main.js"></script>

      </body>
    </html>
END;
    print $html;
  }
}
?>
