<?php
namespace CrazyCharlyDay\Vue;

use CrazyCharlyDay\Controleur as c ;

class VueCalendar extends Vue{

  private $selector ;

  private $deplacer ;
  private $editer ;

  const VUE_VISITEUR = 0 ;
  const VUE_RESERVER = 1 ;


  public function __construct($s){
    parent::__construct();
    $this->selector = $s ;
  }

  private function htmlVisiteur(){
      $this->deplcer = "selectable: false";
      $this->editer = "editable: false" ;
  }
  public function reserver(){


    $this->deplcer = "selectable: true";
    $this->editer = "editable: true" ;
  }

public function render(){


  switch ($this->selector) {
    case 0:
      $this->htmlVisiteur() ;
      break;
    case 1 :
      $this->reserver();
      break;
    default:
      break;
  }


  $html=<<<END
  <!DOCTYPE html>
  <html>
  <head>
  <meta charset='utf-8' />
  <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
  <link rel="stylesheet" href="www/assets/css/main.css" />
  <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
  <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
  <link href='www/calendar/fullcalendar.min.css' rel='stylesheet' />
  <link href='www/calendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
  <script src='www/calendar/lib/moment.min.js'></script>
  <script src='www/calendar/lib/jquery.min.js'></script>
  <script src='www/calendar/fullcalendar.min.js'></script>
  <script src='www/calendar/locale-all.js'></script>
  <script>
    $(document).ready(function() {
      var initialLocaleCode = 'fr';
      $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: '02-12-2018',
        locale: initialLocaleCode,
        navLinks: true, // can click day/week names to navigate views
        $this->editer,
        selectHelper: true,
        select: function(start, end) {
          var title = prompt('Event Title:');
          var eventData;
          if (title) {
            eventData = {
              title: title,
              start: start,
              end: end
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
          }
          $('#calendar').fullCalendar('unselect');
        },
        $this->deplcer,
        eventLimit: true, // allow "more" link when too many events
        events: [
        ]
      });
      $.each($.fullCalendar.locales, function(localeCode) {
        $('#locale-selector').append(
          $('<option/>')
            .attr('value', localeCode)
            .prop('selected', localeCode == initialLocaleCode)
            .text(localeCode)
        );
      });
    });
  </script>
  </head>
  <body>
    <!-- Page Wrapper -->
      <div id="page-wrapper">
        <!-- Header -->
          <header id="header" class="alt">
            <h1><a href="index.html">Solid State</a></h1>
            <nav>
              <a href="#menu">Menu</a>
            </nav>
          </header>

        <!-- Menu -->
          <nav id="menu">
            <div class="inner">
              <h2>Menu</h2>
              <ul class="links">
                <li><a href=$this->url_accueil>Accueil</a></li>
              </ul>
              <a href="#" class="close">Close</a>
            </div>
          </nav>

        <!-- Banner -->
          <section id="banner">
            <div class="inner">
              <div class="logo"><span class="icon fa-calendar"></span></div>
              <h2>Planning Manager</h2>
            </div>
            <div id='calendar'></div>
          </section>
        <!-- Footer -->
        <section id="footer">
            <div class="inner">
              <ul class="copyright">
                <li>&copy; Untitled Inc. All rights reserved.</li>
              </ul>
            </div>
          </section>
      </div>
    <script src="www/assets/js/skel.min.js"></script>
    <script src="www/assets/js/util.js"></script>
    <script src="www/assets/js/jquery.scrollex.min.js"></script>
    <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="www/assets/js/main.js"></script>
    <style>
     /* body {
        margin: 40px 10px;
        padding: 0;
        font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
        font-size: 14px;
      }*/
      #calendar {
        max-width: 900px;
        margin: auto auto;
      }
    </style>
  </body>
  </html>

END;

return $html ;
}



}
