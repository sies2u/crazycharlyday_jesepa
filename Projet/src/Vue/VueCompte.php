<?php
namespace CrazyCharlyDay\Vue;

use CrazyCharlyDay\Modele as m ;
use CrazyCharlyDay\Controleur as c;

class VueCompte extends Vue{

  protected $selector ;

  private $navbar;
  private $script ;
  private $css ;
  private $tableau;

  const VUE_INSCRITPION = 0  ;
  const VUE_CONNEXION = 1 ;
  const VUE_MODIFICATION = 2 ;
  const VUE_UTILISATEUR = 3 ;

  public function __construct($s,$tableau){
    parent::__construct();
    $this->selector = $s ;
    $this->tableau = $tableau;
  }



  private function htmlInscription(){

    $url = $GLOBALS['app']->urlFor('test_inscription') ;

    $this->navbar = <<<END
    <li><a href=$this->url_accueil>Accueil</a></li>
    <li><a href=$this->url_connexion>Connexion</a></li>
END;

  $this->css=<<<END
  <title>Garage Planning Manager</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!--[if lte IE 8]><script src="www/assets/js/ie/html5shiv.js"></script><![endif]-->
  <link rel="stylesheet" href="www/assets/css/main.css" />
  <!--[if lte IE 9]><link rel="stylesheet" href="www/assets/css/ie9.css" /><![endif]-->
  <!--[if lte IE 8]><link rel="stylesheet" href="www/assets/css/ie8.css" /><![endif]-->
END;

$this->script=<<<END
<!-- Scripts -->
  <script src="www/assets/js/skel.min.js"></script>
  <script src="www/assets/js/jquery.min.js"></script>
  <script src="www/assets/js/jquery.scrollex.min.js"></script>
  <script src="www/assets/js/util.js"></script>
  <!--[if lte IE 8]><script src="www/assets/js/ie/respond.min.js"></script><![endif]-->
  <script src="www/assets/js/main.js"></script>

END;
    $html=<<<END


    <h1>
        <center>Page d'inscription</center>
    </h1>

    <form action=$url method="post">
    <p style="width:80%;padding-left:10%;padding-right:10%">
      <label for ="nom"> Nom:</label>
      <input type="text" name="nom" required/> </BR>
      <label for ="login"> Login:</label>
      <input type="text" name="login" required/> </BR>
      <label for ="email"> email:</label>
        <input type="email" name="email"  placeholder="exemple@gmail.com"  required/> </BR>
    	<label for ="mdp"> mot de passe:</label>
    	<input type="password" name="mdp"  required /> </BR>

        <input type="submit" value="S'inscrire" />
    </p>
    </form>




END;

  return $html ;

  }

  private function visualiserUtilisateur(){

    if(c\Authentification::verificationConnexion()){
      $this->navbar = <<<END
      <li><a href="$this->home">Accueil</a></li>
      <li><a href="$this->url_deconnexion">Deconnexion</a></li>
END;
    }else{
      $this->navbar = <<<END
      <li><a href="$this->url_accueil">Accueil</a></li>
      <li><a href="$this->url_connexion">Connexion</a></li>
      <li><a href="$this->url_inscription">Inscription</a></li>
END;
    }

    $this->css=<<<END
  <title>Garage Planning Manager</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!--[if lte IE 8]><script src="www/assets/js/ie/html5shiv.js"></script><![endif]-->
  <link rel="stylesheet" href="www/assets/css/main.css" />
  <!--[if lte IE 9]><link rel="stylesheet" href="www/assets/css/ie9.css" /><![endif]-->
  <!--[if lte IE 8]><link rel="stylesheet" href="www/assets/css/ie8.css" /><![endif]-->
END;

    $this->script=<<<END
<!-- Scripts -->
  <script src="www/assets/js/skel.min.js"></script>
  <script src="www/assets/js/jquery.min.js"></script>
  <script src="www/assets/js/jquery.scrollex.min.js"></script>
  <script src="www/assets/js/util.js"></script>
  <!--[if lte IE 8]><script src="www/assets/js/ie/respond.min.js"></script><![endif]-->
  <script src="www/assets/js/main.js"></script>
END;
    $html="<center><h2>Liste de tous les utilisateurs : </h2>";
    $i = 1;
    foreach ($this->tableau as $value) {
      $html = $html."<h2>Utilisateur numéro ".$i."</h2>";
      $nom = "<h3>Nom   : </h3>".$value['nom'];
      $val = "<h3>Email : </h3>".$value['email'];
      $html = $html."<p>$nom</p><p>$val</p>";
      $i++;
    }
    $html= $html."</center>";
    return $html ;
  }

  private function htmlConnexion(){
    $url = $GLOBALS['app']->urlFor('test_connexion') ;
    $this->navbar = <<<END
    <li><a href=$this->url_accueil>Accueil</a></li>
    <li><a href=$this->url_inscription>Inscription</a></li>
END;

$this->css=<<<END
<title>Garage Planning Manager</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if lte IE 8]><script src="www/assets/js/ie/html5shiv.js"></script><![endif]-->
<link rel="stylesheet" href="www/assets/css/main.css" />
<!--[if lte IE 9]><link rel="stylesheet" href="www/assets/css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="www/assets/css/ie8.css" /><![endif]-->
END;

$this->script=<<<END
<!-- Scripts -->
  <script src="www/assets/js/skel.min.js"></script>
  <script src="www/assets/js/jquery.min.js"></script>
  <script src="www/assets/js/jquery.scrollex.min.js"></script>
  <script src="www/assets/js/util.js"></script>
  <!--[if lte IE 8]><script src="www/assets/js/ie/respond.min.js"></script><![endif]-->
  <script src="www/assets/js/main.js"></script>
END;

    $html=<<<END
    <h1>
        <center>Page de connexion</center>
    </h1>

    <form action=$url method="post">
    <p style="width:80%;padding-left:10%;padding-right:10%">
      <label for ="login"> Login:</label>
      <input type="text" name="login" required/> </BR>

    	<label for ="mdp"> mot de passe:</label>
    	<input type="password" name="mdp"  required /> </BR>

        <input type="submit" value="Se connecter" />
    </p>
    </form>
END;

  return $html ;

  }


  private function htmlModification(){

    $url = $GLOBALS['app']->urlFor('test_compte_modification') ;

    $user = m\User::where('login','=',$_SESSION['login'])->first();

    $l = $user['login'];
    $n = $user['nom'] ;
    $e = $user['email'] ;

    $this->navbar = <<<END
        <li><a href=$this->url_accueil>Accueil</a></li>
        <li><a href=$this->url_inscription>Inscription</a></li>
END;

$this->css=<<<END
<title>Garage Planning Manager</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if lte IE 8]><script src="../www/assets/js/ie/html5shiv.js"></script><![endif]-->
<link rel="stylesheet" href="../www/assets/css/main.css" />
<!--[if lte IE 9]><link rel="../stylesheet" href="www/assets/css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="../stylesheet" href="www/assets/css/ie8.css" /><![endif]-->
END;

$this->script=<<<END
<!-- Scripts -->
  <script src="../www/assets/js/skel.min.js"></script>
  <script src="../www/assets/js/jquery.min.js"></script>
  <script src="../www/assets/js/jquery.scrollex.min.js"></script>
  <script src="../www/assets/js/util.js"></script>
  <!--[if lte IE 8]><script src="../www/assets/js/ie/respond.min.js"></script><![endif]-->
  <script src="../www/assets/js/main.js"></script>
END;

    $html=<<<END


    <h1>
        <center>Modification</center>
    </h1>

    <form action=$url method="post">
    <p style="width:80%;padding-left:10%;padding-right:10%">
    <label for ="nom"> Nom:</label>
    <input type="text" name="nom" value=$n required/> </BR>
    <label for ="login"> Login:</label>
    <input type="text" name="login" value=$l required/> </BR>
    <label for ="email"> email:</label>
      <input type="email" name="email"  value=$e  required/> </BR>
    <label for ="mdp"> mot de passe:</label>
    <input type="password" name="mdp"  required /> </BR>

      <input type="submit" value="Enregistrer" />
    </p>
    </form>


END;

  return $html ;

  }



public function render(){
    $content = '' ;

    switch ($this->selector) {
      case self::VUE_CONNEXION:
        $content = $this->htmlConnexion();
        break ;
      case self::VUE_INSCRITPION:
        $content = $this->htmlInscription() ;
        break;
      case self::VUE_MODIFICATION:
        $content = $this->htmlModification();
        break;
      case self::VUE_UTILISATEUR:
        $content = $this->visualiserUtilisateur();
        break;
      default:
        $content = 'erreur' ;
        break;
    }

    $user = "";
    if(c\Authentification::verificationConnexion()){
      $user = $_SESSION['login'];
    }else{
      $user = "Garage Planning Manager";
    }

    $html = <<<END
  <!DOCTYPE HTML>
  <html>
    <head>
      $this->css
    </head>
    <body>

      <!-- Page Wrapper -->
        <div id="page-wrapper">

          <!-- Header -->
            <header id="header" class="alt">
              <h1><a>$user</a></h1>
              <nav>
                <a href="#menu">Menu</a>
              </nav>
            </header>

          <!-- Menu -->
            <nav id="menu">
              <div class="inner">
                <h2>Menu</h2>
                <ul class="links">
                  $this->navbar
                </ul>
                <a href="#" class="close">Close</a>
              </div>
            </nav>

          <!-- Banner -->
            <section id="banner">
              <div class="inner">
                <div class="logo"><span class="icon fa-car"></span></div>
                <h2>Garage Planning Manager</h2>
              </div>
            </section>

          <!-- Wrapper -->
            <section id="wrapper">

              $content

            </section>

        </div>

    $this->script

    </body>
  </html>
END;

    return $html ;



}

}
