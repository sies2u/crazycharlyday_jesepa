<?php
namespace CrazyCharlyDay\Controleur;

use CrazyCharlyDay\Modele as m;
use CrazyCharlyDay\Vue as v;
use CrazyCharlyDay\Controleur as c ;

class ControleurCalendar{

  public static function pageCalendar(){
    $vc = new v\VueCalendar( v\VueCalendar::VUE_VISITEUR,null) ;
    echo $vc->render() ;
  }

  public static function reserver(){
    $reservation = m\Reservation::get();
    foreach ($reservation->toArray() as $value) {
      $id = $value['id'];
      $titre = $value['titre'];
      $start = $value['start'];
      $end = $value['end'];
      $url = $value['url'];
      $arrayName = array('id' => $id,'titre' => $titre, 'start' => $start, 'end' => $end , 'url' => $url);
      $items[] = $arrayName;
    }
    $json = json_encode($items);
    $vue = new v\VueCalendar(v\VueCalendar::VUE_RESERVER,$json);
    echo $vc->render();
  }
}

 ?>
