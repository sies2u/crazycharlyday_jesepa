<?php

namespace CrazyCharlyDay\Controleur;


class Authentification{


  public static function connexion($id , $login){
    $_SESSION['id'] = $id ;
    $_SESSION['login'] = $login ;
    $_SESSION['droit'] = 1 ;
  }


  public static function deconnexion(){
      session_destroy();
  }

  public static function verificationConnexion(){
    if(isset($_SESSION['id']) ){
      return true ;
    }
    else{
      return false ;
    }
  }

  public static function verificationDroit($d){
      if(isset($_SESSION['droit']) ){
          if($_SESSION['droit'] == $d){
            return true ;
          }
          else{
            return false;
          }
      }
      else{
        return false ;
      }

  }

}


 ?>
