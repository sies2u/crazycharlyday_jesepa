<?php

namespace CrazyCharlyDay\Controleur;

use CrazyCharlyDay\Vue as v;
use CrazyCharlyDay\Modele as m;
use CrazyCharlyDay\Controleur as c;

class ControleurCompte{

  public static function creer($n,$e,$l,$m){

    $c = new m\User() ;

    $c->nom = $n ;
    $c->email = $e ;
    $c->login = $l ;
    $c->mdp = $m ;

    $c->save() ;
  }


  public static function modifier($id,$n,$e,$l,$m){
    $c = m\User::find($id) ;

    $c->nom = $n ;
    $c->email = $e ;
    $c->login = $l ;
    $c->mdp = $m ;

    $c->save() ;

  }

  public static function visualiserUtilisateur(){
    $users = m\User::get();
    $vue = new v\VueCompte(v\VueCompte::VUE_UTILISATEUR,$users->toArray());
    echo $vue->render();
  }

  public static function testInscription(){

    $l = filter_var($_POST['login'] ,FILTER_SANITIZE_STRING);
    $e = filter_var($_POST['email'] ,FILTER_SANITIZE_STRING);
    $n = filter_var($_POST['nom'] ,FILTER_SANITIZE_STRING);

    $m = $mdp=password_hash($_POST['mdp'], PASSWORD_DEFAULT , ['cost'=>12]);


    $nbl = m\User::where('login','=',$l)->count() ;
    $nbe = m\User::where('email','=',$e)->count() ;

    if($nbl==0){

      if($nbe==0){

      self::creer($n,$e,$l,$m) ;
      $url = $GLOBALS['app']->urlFor('accueil') ;

      header("Location: $url");
      exit();
      }
      else{
        self::pageInscription();
        echo 'erreur : un compte existe déjà avec cet email' ;
      }

    }
    else{
        self::pageInscription();
        echo 'erreur : Login indisponible' ;
    }

  }

  public static function testConnexion(){

    $l = filter_var($_POST['login'] ,FILTER_SANITIZE_STRING);
    $mdp = filter_var($_POST['mdp'] ,FILTER_SANITIZE_STRING);

    $nbl = m\User::where('login','=',$l)->count() ;

    if($nbl > 0 ){

      $u = m\User::where('login','=',$l)->first() ;

      if (password_verify($mdp, $u['mdp'] ) ){
        c\Authentification::connexion($u['id'] , $l) ;

        $url = $GLOBALS['app']->urlFor('accueil') ;

        header("Location: $url");
        exit();
      }
      else{
        self::pageConnexion() ;
        echo "erreur : mot de passe incorrect" ;
      }
    }
    else{
      self::pageConnexion();
      echo "erreur : login inexstant" ;
    }

  }

  public static function testModification(){

    $l = filter_var($_POST['login'] ,FILTER_SANITIZE_STRING);
    $n = filter_var($_POST['nom'] ,FILTER_SANITIZE_STRING);
    $e = filter_var($_POST['email'] ,FILTER_SANITIZE_STRING);



    $m = $mdp=password_hash($_POST['mdp'], PASSWORD_DEFAULT , ['cost'=>12]);

    $nbe = m\User::where('email','=',$e)->count() ;

    $nbl = m\User::where('login','=',$l)->count() ;


    if($nbl==0 || $l==$_SESSION['login'] ){

      if($nbe==0 || $e == m\User::where('id','=',$_SESSION['id'])->first()['email']  ){

      self::modifier($_SESSION['id'],$n, $e ,$l,$m) ;
      c\Authentification::connexion($_SESSION['id'] ,$l) ;


      $url = $GLOBALS['app']->urlFor('accueil') ;

      header("Location: $url");
      exit();

    }
    else{
      self::pageModification();
      echo 'erreur : un compte existe déjà avec cet email' ;
    }

    }
    else{
        self::pageModification();
        echo 'erreur : Login indisponible' ;
    }

  }

  public static function deconnexion(){
    c\Authentification::deconnexion();
    $url = $GLOBALS['app']->urlFor('accueil') ;

    header("Location: $url");
    exit();
  }


  public static function pageConnexion(){
    $vc = new v\VueCompte( v\VueCompte::VUE_CONNEXION , null) ;
    echo $vc->render();
  }

  public static function pageInscription(){

    $vc = new v\VueCompte( v\VueCompte::VUE_INSCRITPION , null) ;
    echo $vc->render() ;

  }


  public static function pageModification(){

    $vc = new v\VueCompte( v\VueCompte::VUE_MODIFICATION , null) ;
    echo $vc->render() ;

  }




}
 ?>
