<?php
namespace CrazyCharlyDay\Controleur;
use CrazyCharlyDay\Modele as m;
use CrazyCharlyDay\Vue as v;
class ControleurCategorie{
  public static function visualiserItems($id){
    $categorie = m\Categorie::where("id","=",$id)->first();
    $items = $categorie->items;
    $vue = new v\VueCategorie($categorie,$items);
    $vue->render(1);
  }
}

 ?>
