<?php
namespace CrazyCharlyDay\Controleur;
use CrazyCharlyDay\Vue as v;
use CrazyCharlyDay\Modele as m;
class ControleurAccueil{
  public static function pageAccueil(){
    $liste = m\Categorie::get();
    $vue = new v\VueAccueil($liste->toArray());
    $vue->render();
  }
}
 ?>
