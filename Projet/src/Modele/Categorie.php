<?php
namespace CrazyCharlyDay\Modele;
use CrazyCharlyDay\Modele as m;
class Categorie extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'categorie';
  // Clé primaire : no
  protected $primaryKey = 'id';
  public $timestamps = false;
  public function items(){
    return $this->hasMany('CrazyCharlyDay\Modele\Item','id_categ');
  }
}
 ?>
