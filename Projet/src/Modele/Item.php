<?php
namespace CrazyCharlyDay\Modele;
class Item extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'item';
  // Clé primaire : no
  protected $primaryKey = 'id';
  public $timestamps = false;
  public function categorie(){
    return $this->belongsTo('CrazyCharlyDay\Modele\Categorie','id_categ');
  }
  public function reservation(){
    return $this->hasMany('CrazyCharlyDay\Modele\Item','id_item');
  }
  public function user(){
    return $this->hasMany('CrazyCharlyDay\Modele\Item','id_user');
  }
}
 ?>
