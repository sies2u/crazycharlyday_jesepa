<?php
namespace CrazyCharlyDay\Modele;
class User extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'user';
  // Clé primaire : no
  protected $primaryKey = 'id';
  public $timestamps = false;
  public function item(){
    return $this->belongsTo('CrazyCharlyDay\Modele\Item','id_item');
  }
  public function utilisateur(){
    return $this->belongsTo('CrazyCharlyDay\Modele\User','id_user');
  }
}
 ?>
