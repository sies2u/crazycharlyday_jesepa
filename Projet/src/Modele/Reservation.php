<?php
namespace CrazyCharlyDay\Modele;
class Reservation extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'reservation';
  // Clé primaire : no
  protected $primaryKey = 'id';
  public $timestamps = false;
  
}
 ?>
